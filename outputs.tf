output "IP_Priv" {
    value = [
        for ippriv in oci_core_instance.CLR_LNX:
            ippriv.private_ip
    ]
}

output "host_e_ip" {
    value = {
      nomedohost   = oci_core_instance.CLR_LNX[*].display_name
      ipdohost     = oci_core_instance.CLR_LNX[*].private_ip
    }
}
output "hostname_label" {
    value = [
        for hsnm in oci_core_instance.CLR_LNX:
            hsnm.display_name
    ]
}

output "compilado" {
    value = zipmap(tolist([for hs in oci_core_instance.CLR_LNX: hs.display_name]), tolist([for ippriv in oci_core_instance.CLR_LNX: ippriv.private_ip]))
}