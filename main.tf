resource "oci_core_instance" "CLR_LNX" {
    count = var.qtdemaq
    agent_config {
    is_management_disabled = "false"
    is_monitoring_disabled = "false"
    plugins_config {
      desired_state = "ENABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Oracle Java Management Service"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Oracle Autonomous Linux"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Bastion"
    }
  }
  availability_config {
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.AvDomain
  compartment_id      = var.compartment_id
  create_vnic_details {
    assign_private_dns_record = var.private_dns
    assign_public_ip          = var.ip_publico
    subnet_id                 = var.subnet-sp-priv
  }
  display_name = "${var.projeto}-${count.index}"
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  is_pv_encryption_in_transit_enabled = "false"
  metadata = {
    "ssh_authorized_keys" = var.pubkB
  }
  shape = var.shape
  shape_config {
    memory_in_gbs = var.MEM
    ocpus         = var.CPU
  }
  source_details {
    source_id   = var.ImageID
    source_type = "image"
  }
  freeform_tags = local.common_tags
  lifecycle {
    ignore_changes = [
      is_pv_encryption_in_transit_enabled
    ]
  }
}

